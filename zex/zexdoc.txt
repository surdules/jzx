Z80doc instruction exerciser

<adc,sbc> hl,<bc,de,hl,sp>....OK
add hl,<bc,de,hl,sp>..........OK
add ix,<bc,de,ix,sp>..........OK
add iy,<bc,de,iy,sp>..........OK
aluop a,nn....................OK
aluop a,<b,c,d,e,h,l,(hl),a>..   CRC:3cda7271 expected:fe43b016
aluop a,<ixh,ixl,iyh,iyl>.....OK
aluop a,(<ix,iy>+1)...........   CRC:69a3ee81 expected:e849676e
bit n,(<ix,iy>+1).............   CRC:ca69ee10 expected:a8ee0867
bit n,<b,c,d,e,h,l,(hl),a>....   CRC:01f4bf98 expected:7b55e6c8
cpd<r>........................   CRC:8702e411 expected:a87e6cfa
cpi<r>........................   CRC:ff4ba887 expected:06deb356
<daa,cpl,scf,ccf>.............OK
<inc,dec> a...................OK
<inc,dec> b...................OK
<inc,dec> bc..................OK
<inc,dec> c...................OK
<inc,dec> d...................OK
<inc,dec> de..................OK
<inc,dec> e...................OK
<inc,dec> h...................OK
<inc,dec> hl..................OK
<inc,dec> ix..................OK
<inc,dec> iy..................OK
<inc,dec> l...................OK
<inc,dec> (hl)................   CRC:8cb36ec8 expected:b83adcef
<inc,dec> sp..................OK
<inc,dec> (<ix,iy>+1).........   CRC:d4adaaff expected:20581470
<inc,dec> ixh.................OK
<inc,dec> ixl.................OK
<inc,dec> iyh.................OK
<inc,dec> iyl.................OK
ld <bc,de>,(nnnn).............OK
ld hl,(nnnn)..................OK
ld sp,(nnnn)..................OK
ld <ix,iy>,(nnnn).............OK
ld (nnnn),<bc,de>.............OK
ld (nnnn),hl..................OK
ld (nnnn),sp..................OK
ld (nnnn),<ix,iy>.............OK
ld <bc,de,hl,sp>,nnnn.........OK
ld <ix,iy>,nnnn...............OK
ld a,<(bc),(de)>..............   CRC:3bc66a71 expected:b0818935
ld <b,c,d,e,h,l,(hl),a>,nn....OK
ld (<ix,iy>+1),nn.............   CRC:e80cff11 expected:26db477e
ld <b,c,d,e>,(<ix,iy>+1)......   CRC:360f0182 expected:cc1106a8
ld <h,l>,(<ix,iy>+1)..........   CRC:b7ba0c9c expected:fa2a4d03
ld a,(<ix,iy>+1)..............   CRC:e75a1e63 expected:a5e9ac64
ld <ixh,ixl,iyh,iyl>,nn.......OK
ld <bcdehla>,<bcdehla>........   CRC:437589be expected:744b0118
ld <bcdexya>,<bcdexya>........   CRC:f163ae1a expected:478ba36b
ld a,(nnnn) / ld (nnnn),a.....OK
ldd<r> (1)....................   CRC:39e295ed expected:94f42769
ldd<r> (2)....................   CRC:f786cc50 expected:5a907ed4
ldi<r> (1)....................   CRC:37ab4431 expected:9abdf6b5
ldi<r> (2)....................   CRC:464f3b9f expected:eb59891b
neg...........................OK
<rrd,rld>.....................   CRC:71659800 expected:955ba326
<rlca,rrca,rla,rra>...........OK
shf/rot (<ix,iy>+1)...........   CRC:4c3caabb expected:713acd81
shf/rot <b,c,d,e,h,l,(hl),a>..   CRC:79f90aa8 expected:eb604d58
<set,res> n,<bcdehl(hl)a>.....   CRC:775e8975 expected:8b57f008
<set,res> n,(<ix,iy>+1).......   CRC:fa9f51d4 expected:cc63f98a
ld (<ix,iy>+1),<b,c,d,e>......   CRC:dc962025 expected:04626abf
ld (<ix,iy>+1),<h,l>..........   CRC:278ac9ae expected:6a1a8831
ld (<ix,iy>+1),a..............   CRC:d1022a13 expected:ccbe5a96
ld (<bc,de>),a................   CRC:19caed2f expected:7a4c114f
Tests complete
