# TODO #

## HIGH PRIORITY ##
* Unified parameter handling model
* Add parameters:
    * full speed (no frame delay, no speaker, no AY)
    * frame delay (no speaker)
    * no speaker
    * no AY
* Z80 undocumented flags for I/O block instructions (http://www.z80.info/z80undoc3.txt)

## MEDIUM PRIORITY ##
* Re-design AY/Speaker to queue and not block CPU
* Improve drawImage() performance; maybe use VolatileImage?
* Loader factory
* Load
    * SNA
    * SLT
    * TAP (Z80 load/save/trap)
    * TZX
* Dialog
    * Load, Save
    * Reset, NMI
    * Pause, Speed
    * Sound (enable/disable)
    * Hardware mode (48/128)
    * Joystick keyboard mapping
* Create dummy objects instead of returning null sound streamers

# LOW PRIORITY #
* Auto-configure parameters in AY8912 and Speaker
* Handle 16-bit mode in ScreenJava2

# Failures due to documented and undocumented flags #

	<daa,cpl,scf,ccf>.............OK

Failures due to undocumented flags (@ = failed):

	bit n,(<ix,iy>+1)............   CRC:e1d4a896 expected:83534ee1
	@bit n,<b,c,d,e,h,l,(hl),a>...   CRC:f554d742 expected:5e020e98
	@cpd<r>.......................   CRC:d06b9c15 expected:134b622d
	@cpi<r>.......................   CRC:683e7b2a expected:2da42d19
	ldd<r> (1)...................   CRC:39e295ed expected:94f42769
	@ldd<r> (2)...................   CRC:94cb8f65 expected:39dd3de1
	@ldi<r> (1)...................   CRC:5a940255 expected:f782b0d1
	ldi<r> (2)...................   CRC:44fc622a expected:e9ead0ae
